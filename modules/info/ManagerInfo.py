"""
    Store information about the current General Manager in use.

    * version - version that Manager is running on
    * man_type - type of manager that is used
"""

class ManagerInfo:

    def __init__(self, version, man_type):
        self.version = version
        self.man_type = man_type

    def get_version(self):
        return self.version

    def get_manager_type(self):
        return self.man_type
