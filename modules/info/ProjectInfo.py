"""
    Store information about the current project being evaluated

    * project_title - title of the project
    * date - date that the project is being review
"""


class ProjectInfo:

    def __init__(self, date):
        self.date = date
        self.project_title = ''
        self.location = ''
        self.geo_market = ''
        self.customer = ''
        self.job_type = ''
        self.well_name = ''
        self.version = ''
        self.engineer = ''

    def set_title_project(self, project):
        self.project_title = project

    def get_title_project(self):
        return self.project_title

    def get_date(self):
        return self.date

    def set_date(self, date):
        self.date = date
        
    def set_location(self, location):
        self.location = location
        
    def get_location(self):
        return self.location
        
    def set_geomarket(self, market):
        self.geo_market = market
        
    def get_geomarket(self):
        return self.geo_market
        
    def set_customer(self, customer):
        self.customer = customer
        
    def get_customer(self):
        return self.customer
        
    def set_job_type(self, job):
        self.job_type = job
        
    def get_job_type(self):
        return self.job_type
        
    def set_well_name(self, well):
        self.well_name = well
        
    def get_well_name(self):
        return self.well_name
        
    def set_version(self, version):
        self.version = version
     
    def get_version(self):
        return self.version
        
    def set_engineer(self, name):
        self.engineer = name
        
    def get_engineer(self):
        return self.engineer
        
    def struct_data(self):
        struct_info = [
            ('Title',self.project_title),
            ('Date', 'Temp Date'),
            ('Geo-market', self.geo_market),
            ('Location', self.location),
            ('Customer', self.customer),
            ('Job Type', self.job_type),
            ('Well Name', self.well_name),
            ('Version', self.version),
            ('SLB Engineer', self.engineer),
        
            ]
        return struct_info