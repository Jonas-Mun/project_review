from tkinter import *


class ViewQuestionnaireCMD():

    def display(self, prompt):
        ROW_LENGTH = 20

        SYMBOL_BORDER_PROMPT = "*"
        SYMBOL_BORDER_HEADER = "-"

        # Display top border
        print(SYMBOL_BORDER_HEADER * ROW_LENGTH)

        # Display Header info
        self.display_prompt_info(prompt.getCategory(), prompt.getGroup(), ROW_LENGTH)

        print(SYMBOL_BORDER_PROMPT * ROW_LENGTH)
        # Display Text Prompt
        self.display_prompt(prompt, ROW_LENGTH, SYMBOL_BORDER_PROMPT)

        # Display Bottom Border
        print(SYMBOL_BORDER_PROMPT * ROW_LENGTH)

    def display_prompt_info(self, cat, group, row_length):
        self.show_text(cat, 10, "-", "")
        self.show_text(group, 10, "-", "\n")

    def display_prompt(self, prompt, row_length, symbol_border):
        self.show_text(prompt.getPrompt(), row_length, symbol_border, "\n")

    def summary_display(self, prompt):
        self.display(prompt)
        print("Status :" + str(prompt.getStatus()))


    def show_text(self, text, row_length, symbol_border, the_end):
        MID_POINT = row_length / 2

        GAP = " "

        # Calculate text position
        text_length = len(text)
        half_length = text_length / 2

        # Center align the text
        position = MID_POINT - half_length

        # Gaps to add
        BEGINNING_CHARS = 1

        gaps = position - BEGINNING_CHARS

        # Display text
        end_of_row = ""
        if (int(half_length) % 2 == 0): # even length
            end_of_row = GAP + symbol_border
        else:
            end_of_row = symbol_border
            if (the_end == ""):
                end_of_row = GAP + end_of_row

        print(symbol_border + (GAP * int(gaps)) + str(text) + (GAP * int(gaps)) + end_of_row, end=the_end)

    def display_error(self):
        print("Invalid input, try again")



class ViewQuestionnaireGUI:
    def __init__(self, master, prompt):
        self.prompt = prompt
        frame = Frame(master)
        frame.pack()

        self.yes_button = Button(frame, text="Yes", command=lambda: prompt.updateStatus("y"))
        self.yes_button.pack(side=LEFT)

        self.no_button = Button(frame, text="No", command=lambda: prompt.updateStatus("n"))
        self.no_button.pack(side=LEFT)

        self.next_button = Button(frame, text="NEXT", command=frame.quit)
        self.next_button.pack(side=LEFT)


    def display_prompt(self, prompt):
        self.yes = tk.Button(self, width=25)
        self.yes["text"] = "Yes"
        self.yes["command"] = prompt.updateStatus("y")
        self.yes.pack()

        self.no = tk.Button(self, width=25)
        self.no["text"] = "No"
        self.no["command"] = prompt.updateStatus("n")
        self.no.pack()


        self.na = tk.Button(self, width=25)
        self.na["text"] = "N/A"
        self.na["command"] = prompt.updateStatus("na")
        self.na.pack()

        self.NEXT = tk.Button(self, width=25,text="NEXT", fg="red", command=self.master.destroy)
        self.NEXT.pack()


