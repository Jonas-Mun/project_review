from reportlab.platypus import SimpleDocTemplate
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import Table
from reportlab.platypus import TableStyle
from reportlab.platypus import Image
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.platypus import Spacer

class WinReportLab:
    def __init__(self, orient, info):
        self.size = (0,0)
        self.styles = getSampleStyleSheet()
        if (orient == "landscape"):
            self.size = (841.89, 595.27)
        else:
            self.size=(595.27, 841.89)
			
        self.PAGE_HEIGHT = self.size[1]
        self.PAGE_WIDTH = self.size[0]
        self.title = 'Bogus'
        
        self.info = info

    def set_data(self, data):
        self.data = data
        
    def set_info(self, info):
        self.info = info
		
    def myFirstPage(self, canvas, doc):
        project_info = "protoype"
        canvas.saveState()
        canvas.setFont('Times-Bold', 16)
        canvas.drawCentredString((self.PAGE_WIDTH)/2.0,(self.PAGE_HEIGHT-108), self.title)
        canvas.setFont('Times-Roman', 8)
        
        # Footer
        canvas.drawString(cm, 0.75*cm, "First Page / %s" % project_info)
        canvas.drawString((self.PAGE_WIDTH-60)/2.0,0.75*cm, "Schlumberger-Private")
        canvas.drawString((self.PAGE_WIDTH*0.75), 0.75*cm, 'References: ')
        
        # draw info
        y_start_position = 500
        y_current = y_start_position
        
        y_step = 15
        
        x_position = 100
        canvas.setFont('Times-Roman', 9)
        print(self.info)
        for i in range(0, len(self.info)):
            canvas.drawString(x_position, y_current, self.info[i][0])    # Header
            x_offset = len(self.info[i][0]) + 15
            canvas.drawString(x_position + 80, y_current, self.info[i][1])    # Info
            
            y_current = y_current - y_step
            
        
        
        
        canvas.restoreState()
	
    def myLaterPages(self, canvas, doc):
        project_info = "prototype"
        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawString(cm, 0.75 * cm, "Page %d %s" % (doc.page, project_info))
        canvas.drawString((self.PAGE_WIDTH-60)/2.0,0.75*cm, "Schlumberger-Private")
        canvas.restoreState()
	
    def make_table(self, data):
        t = Table(data)
        
        ts = TableStyle(
            [
            ('BOX', (0,0),(-1,-1),1,colors.black),
            ('GRID', (0,1),(-1,-1),1,colors.black),
            ]
        )
        t.setStyle(ts)
        
        
        return t
		
    def make_report(self, file_name):
        pdf = SimpleDocTemplate(
            file_name,
            pagesize=self.size
            )
		
        Story = [Spacer(1,6.5*cm)]
        style = self.styles["Normal"]

        table = self.make_table(self.data)
        Story.append(table)
        im = Image('test_summary_graph.png',25*cm, 14*cm)
        Story.append(im)

        pdf.build(Story, onFirstPage=self.myFirstPage, onLaterPages=self.myLaterPages)

