
from .win_report import WinReportLab

class Report:
    def __init__(self, raw_data, rep_type):
        self.raw_data = raw_data
        self.rep_type = rep_type
        
        """
            Test Data when info no set
        """
        self.info = [
        ('Title','Test Data'), 
        ('Date','5/12/2019'), 
        ('Geomarket', 'Market'), 
        ('Location', 'Earth'), 
        ('Customer', 'Shell'), 
        ('Job Type', 'Oil'), 
        ('Well Name', 'Well'), 
        ('Version', 'Test'), 
        ('SLB Engineer', 'Junasleu'),
        ]
        
        if (rep_type == 'win'):
            self.report_gen = WinReportLab("landscape", self.info)
        else:
            self.report_gen = None
        

    def start_win_gen(self):
        data = self.conv_data_report(self.raw_data)
        if (self.file_name == None):
            return False
        else:
            self.report_gen.set_data(data)
            self.report_gen.set_info(self.info)
            self.report_gen.make_report(self.file_name)
            return True

    def set_filename(self, name):
        self.file_name = name
        
    def set_info(self, info):
        self.info = info

    """
        Convert: [[q0,q1],[q2,q3]] -> [[HEADERS], [Q1],[Q2]]
    """
    def conv_data_report(self, raw_data):
        self.report_data = []
        HEADERS = ["Category","Group","Question","Status","Comment"]
        self.report_data.append(HEADERS)
        for cat in raw_data:
            for ques in cat:
                print(ques)
                # Set up a row
                question = []
                question.append(ques.getCategory())
                question.append(ques.getGroup())
                question.append(ques.getPrompt())
                question.append(ques.getStatus())
                question.append(ques.getComment())
                # Append row into report data
                self.report_data.append(question)
        return self.report_data





