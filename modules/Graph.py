import matplotlib.pyplot as plt

import logging

graph_logger = logging.getLogger('manager_application.modules')

class Graph:
    def __init__(self):
        self.logger = logging.getLogger("manager_application.modules.Graph")
        self.logger.info("Graph instance created")
    """
        Receives data of the form:
            {
                cat: [(g,(n,m)),...,]
            }
        Conver to:
            y_axis = [g0,g1,g2,..,gn]
            x_axis = [%y0, %y1, %y2, ...,]
    """
    def convert_data(self, data):
        self.x_axis = []
        self.y_axis = []

        for cat in data:
            for group in data[cat]: # data[cat] == [(grp,(n,m)),... ], group == (grp, (n,m))
                self.y_axis.append(group[0])    # add group name

                status = 1
                yes = 0
                no = 1

                total = group[status][yes] + group[status][no]
                yes_per = (group[status][yes] / total) * 100    # percentage of yes in group

                self.x_axis.append(yes_per)
        self.logger.error("Finished converting data")

    def create_chart(self):
        self.logger.error("Creating Graph")
        plt.rcParams.update({'figure.autolayout': True})

        plt.style.use('seaborn-whitegrid')
        fig, ax = plt.subplots(figsize=(8, 4))
        ax.barh(self.y_axis, self.x_axis)
        #self.logger.error(plt.style.available)

        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45, horizontalalignment='right')
        ax.set(xlim=[0, 100], xlabel=' Yes in Percentage', ylabel='', title="Project Title Summary")
        # Add label after bar
        for i in range(0, len(self.x_axis)):
            offset = 2
            plt.text(self.x_axis[i] + 4, self.y_axis[i], str(self.x_axis[i]) + '%')

        self.logger.error("Finished graph")
        self.fig = fig
        #plt.show()

    def show_graph(self):
        plt.show()

    def close_graph(self):
        plt.close()

    def set_x_y_values(self, x, y):
        self.x_axis = x
        self.y_axis = y

    def store_fig(self, file_type, title):
        if (file_type == "png"):
            self.fig.savefig(title + '.png', transparent=False, dpi=80, bbox_inches="tight")
