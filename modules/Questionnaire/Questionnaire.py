from .views.gui_view import ViewQuestionnaireGUI
from.views.cmd_view import ViewQuestionnaireCMD

from tkinter import *

import logging

logger = logging.getLogger('manager_application.Questionnaire')
logger.info("created Questionnaire logger")

class Questionnaire():
    def __init__(self, event_func):
        self.finished = event_func

    # Main function to run Questionnaire
    def start_questionnaire(self, view_type, master):
        if (view_type == "gui"):
            self.new_window = Toplevel(master)
            q_gui = ViewQuestionnaireGUI(self.new_window, self.prompts, self.update_status, self.summary, self.finished)

        elif (view_type == "cmd"):
            cmd_view = ViewQuestionnaireCMD(self.prompts)
            cmd_view.display_all()
        else:
            logger.error("No view specified")
            print("ERROR: No view specifide")

    def init_prompts(self, prompts):
        self.prompts = prompts
        logger.info("New prompts initialized in questionnaire")

    def empty_prompts(self):
        self.prompts = []

    #----- Functions to pass to View -------

    def update_status(self, prompt,status):
        prompt.updateStatus(status)

    def summary(self, prompts):
        for prompt in prompts:
            self.view.summary_display(prompt)
