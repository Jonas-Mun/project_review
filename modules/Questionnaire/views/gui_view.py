from tkinter import *

DEBUG = False

class ViewQuestionnaireGUI:

    def __init__(self, master, prompts, update_func, summ_func, fin_func):
        self.master = master
        self.prompts = prompts
        self.updateStatus = update_func
        self.summary = summ_func
        self.finished = fin_func
        self.cat_num = len(prompts)
        self.curr_cat = 0
        print(self.prompts[0])

        ## Frames

        # Main frame
        self.main_frame = Frame(self.master, width=900, height=600, colormap="new")
        self.main_frame.pack()

        # Header frame
        self.header_frame = Frame(self.main_frame, padx=5, pady=5)
        if (DEBUG):
            self.header_frame["bg"] = "blue"
        self.header_frame.pack()

        # Prompt frame

        self.prompt_frame = Frame(self.main_frame, padx=5, pady=12)
        if (DEBUG):
            self.prompt_frame["bg"] = "green"
        self.prompt_frame.pack()

        # Button frame
        self.button_frame = Frame(self.main_frame, padx=5, pady=5)
        if (DEBUG):
            self.button_frame["bg"] = "red"
        self.button_frame.pack()

        # -- sub-button frame
        self.sub_button_frame_ans = Frame(self.button_frame, padx=5)
        if (DEBUG):
            self.sub_button["bg"] = "pink"
        self.sub_button_frame_ans.grid(row=0,column=0)

        # -- -- sub-sub-button frame
        self.sub_sub_button_frame_other = Frame(self.sub_button_frame_ans, pady=10, padx=2)
        if (DEBUG):
            self.sub_sub_button_frame_other["bg"] = "green"
        self.sub_sub_button_frame_other.pack(side=LEFT)

        self.sub_sub_button_frame_answer = Frame(self.sub_button_frame_ans, pady=5)
        if (DEBUG):
            self.sub_sub_button_frame_answer["bg"] = "blue"
        self.sub_sub_button_frame_answer.pack(side=RIGHT)

        self.sub_button_frame_meta = Frame(self.button_frame, padx=5)
        if (DEBUG):
            self.sub_button_frame_meta["bg"] = "brown"
        self.sub_button_frame_meta.grid(row=0, column=1)

        # Comment frame
        self.comment_frame= Frame(self.main_frame)
        self.comment_frame.pack()

        # Exit frame
        #self.exit_frame = Frame(self.main_frame)
        #self.exit_frame.pack(side=BOTTOM)

        ## Labels

        # States
        self.index_prompt = 0

        # Category header info

        self.categ_frame = LabelFrame(self.header_frame, text="Category", padx=5, pady=5)
        self.categ_frame.config(font=("Courier", 10))
        self.categ_frame.grid(row=0, column=0)

        self.categ_text = StringVar()
        self.categ_text.set(self.prompts[self.curr_cat][self.index_prompt].getCategory())

        self.label_categ = Label(self.categ_frame, textvariable=self.categ_text)
        self.label_categ.grid(row=0, column=0)

        # Group header info
        self.group_frame = LabelFrame(self.header_frame, text="Group", padx=5, pady=5)
        self.group_frame.config(font=("Courier", 10))
        self.group_frame.grid(row=0, column=1, padx=8)

        self.group_text = StringVar()
        self.group_text.set(self.prompts[self.curr_cat][self.index_prompt].getGroup())

        self.label_group = Label(self.group_frame, textvariable=self.group_text)
        self.label_group.grid(row=0, column=0)

        # Prompt info
        self.prompt_title = LabelFrame(self.prompt_frame, text="Prompt", padx=36 )
        self.prompt_title.config(font=("Courier", 10))
        self.prompt_title.grid(row=0, columnspan=2)

        self.prompt_text = StringVar()
        self.prompt_text.set(self.prompts[self.curr_cat][self.index_prompt].getPrompt())

        self.label_prompt = Label(self.prompt_title, textvariable=self.prompt_text)
        self.label_prompt.config(font=("Default", 10))
        self.label_prompt.grid(row=0, columnspan=2)

        # Status info
        self.status_text = StringVar()
        self.status_text.set(self.prompts[self.curr_cat][self.index_prompt].getStatus())

        self.label_status = Label(self.prompt_title, textvariable=self.status_text)
        self.label_status.grid(row=1, column=0, padx=20)

        ## Entries

        # Comment
        self.comment_frame = LabelFrame(self.comment_frame, text="Comment")
        self.comment_frame.grid(row=0, columnspan=2)

        self.entry_comment = Entry(self.comment_frame, textvariable=self.prompts[self.curr_cat][self.index_prompt].getComment())
        self.entry_comment.pack(padx=8, pady=8)

        ## BUTTONS ##

        # Yes 
        self.yes_button = Button(self.sub_sub_button_frame_answer, text="Yes", command=self.update_yes)
        self.yes_button.pack(pady=2)

        # No
        self.no_button = Button(self.sub_sub_button_frame_answer, text="No", command=self.update_no, padx=13)
        self.no_button.pack(pady=2)

        #N/A
        self.na_button = Button(self.sub_sub_button_frame_other, text="N/A", command=self.update_na)
        self.na_button.pack()

        # Next
        self.next_button = Button(self.sub_button_frame_meta, text="Next", command=self.cycle_prompt)
        self.next_button.pack()

    def update_yes(self):
        self.updateStatus(self.prompts[self.curr_cat][self.index_prompt], "y")

        self.status_text.set(self.prompts[self.curr_cat][self.index_prompt].getStatus())

    def update_no(self):
        self.updateStatus(self.prompts[self.curr_cat][self.index_prompt], "n")
        self.status_text.set(self.prompts[self.curr_cat][self.index_prompt].getStatus())

    def update_na(self):
        self.updateStatus(self.prompts[self.curr_cat][self.index_prompt],"na")
        self.status_text.set(self.prompts[self.curr_cat][self.index_prompt].getStatus())

    def cycle_prompt(self):
        # Store Entries
        new_comment = self.entry_comment.get()
        self.prompts[self.curr_cat][self.index_prompt].updateComment(new_comment)

        self.index_prompt += 1

        if (self.index_prompt >= len(self.prompts[self.curr_cat])): # Still prompts in
            self.index_prompt = 0
            self.curr_cat += 1
            if (self.curr_cat >= self.cat_num): # Still categories in
                self.add_quit()
                self.finished()
                self.destroy_questionnaire()
            else:
                self.update_labels()
        else:
            self.update_labels()

    def update_labels(self):
        self.categ_text.set(self.prompts[self.curr_cat][self.index_prompt].getCategory())
        self.group_text.set(self.prompts[self.curr_cat][self.index_prompt].getGroup())
        self.prompt_text.set(self.prompts[self.curr_cat][self.index_prompt].getPrompt())
        self.status_text.set(self.prompts[self.curr_cat][self.index_prompt].getStatus())
        self.entry_comment.delete(0, END)

    def add_quit(self):
        self.quit_button = Button(self.master, text="DONE", command=self.master.destroy)
        self.quit_button.pack(padx=50, pady=50)

    def destroy_questionnaire(self):
        ## Destroy buttons
        # self.yes_button.destroy()
        # self.no_button.destroy()
        # self.na_button.destroy()
        # self.next_button.destroy()
        self.button_frame.destroy()
        self.header_frame.destroy()
        self.prompt_frame.destroy()
        self.comment_frame.destroy()
        self.main_frame.destroy()









