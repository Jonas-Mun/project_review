| TODO                          | Date-Added     | Status  | Date-Finished |
| ----------------------------- | --------------:| :-----: | ------------: |
| add scrollbar to widgets      | 30/11/2019     |    D    | 2/12/2019     |
| make report dynamic           | 30/11/2019     |    P    |               |
| Ads labels to gui components  | 30/11/2019     |    D    | 2/12/2019     |
| Abstract Gui                  | 1/12/2019      |    D    | 2/12/2019     |
| Add graph to report           | 2/12/2019      |    P    |               |
| Produce error messages in gui | 2/12/2019      |    D    | 3/12/2019     |
| Layout report                 | 2/12/2019      |    P    |               |
| Data obtained via REST/server | 2/12/2019      |    ?    |               |
| Add Exception Handling        | 3/12/2019      |    D    | 3/12/2019     |
| Integrate graph module        | 3/12/2019      |    D    | 4/12/2019     |
| Test progam flow              | 4/12/2019      |    D    | 9/12/2019     |
| Remove hardcode               | 9/12/2019      |    D    | 9/12/2019     |
| Error message: start question | 9/12/2019      |    D    | 9/12/2019     |
| General info: project         | 9/12/2019      |    D    | 9/12/2019     |
| Adjust Label entries in GI    | 9/12/2019      |    P    |               |


### FOR OTHER VERSION - Not a priority
|*Add 'finished report' button  | 4/12/2019      |    P    |               |
| Prompt title project          | 5/12/2019      |    P    |               |
| Prompt new Project upon fin.  | 5/12/2019      |    P    |               |


##### Code:
###### P - Progress
###### D - Done
###### ? - Investigating
###### * - Priority
