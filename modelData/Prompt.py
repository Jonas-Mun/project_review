from abc import ABC, abstractmethod

import logging

prompt_logger = logging.getLogger('manager_application.model_data.prompt')

class Prompt:

    def __init__(self, prompt, category, group):
        self.prompt = prompt
        self.category = category
        self.group = group
        self.status = ''
        super().__init__()

    def getPrompt(self):
        return self.prompt

    def getStatus(self):
        return self.status

    def updateStatus(self, stat):
        self.status = stat

    def getCategory(self):
        return self.category

    def getGroup(self):
        return self.group
