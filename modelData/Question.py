from .Prompt import Prompt

class Question(Prompt):
    def __init__(self, category, group, question):
        super(Question, self).__init__(question, category, group)
        self.comment = ''

    def getComment(self):
        return self.comment

    def updateComment(self, comment):
        self.comment = comment


