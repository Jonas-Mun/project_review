from dataManipulators.ManageSetQuestions import ManageSetQuestions
from dataManipulators.CustomExceptions import *

from questionTranslator import JsonToObject

from modules.Questionnaire.Questionnaire import Questionnaire
from modules.Report.Report import Report

from views.Managers import ManagerView

from modules.info.ProjectInfo import ProjectInfo
from modules.info.ManagerInfo import ManagerInfo
from modules.Graph import Graph

from views.ManagerExceptions import *

from datetime import date

import logging


# create logger with 'manager_application'
logger = logging.getLogger('manager_application')
logger.setLevel(logging.DEBUG)


# create file handler which logs even debug messages
fh = logging.FileHandler('./logs/manager.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


logger.info("******* NEW SESSION  ********")


class GeneralManager:
    def __init__(self, data_file, q_type):
        logger.info("Initializing Manager")
        self.data_file = data_file# file where data lies
        self.q_type = q_type	# type of prompts
        self.promptObjects = []

        ## DATA Layer modules ##
        self.question_set_manager = ManageSetQuestions(data_file, q_type)
        self.converter = JsonToObject(data_file)

        ## INFO modules ##
        self.manager_info = ManagerInfo('v0.1', 'General Manager')
        self.project_info = ProjectInfo(date.today())

        ## Function modules ##
        self.init_graph_module()

        self.cat_summ = {}  # Summary of volatile prompts



    def start_manager(self):
        self.gui_mang = ManagerView(self, "gui")
        self.init_questionnaire(self.finished_questionnaire)
        self.init_report_module()
        self.gui_mang.start_view()
        logger.info('Started Manager')
        # -------------------#
	
    """
        Interface between Data Layer and Application Layer
    """
			
    ## Editing Infos ##
    def edit_project_title(self, title):
        self.project_info.set_project_title(title)

    def edit_date(self, date):
        self.project_info.set_date(date)

    ## Edit Hard Prompts ##

    def add_new_prompt(self, cat, group, question):
        # category and group must be provided
        try:
            if (self.question_set_manager.categoryExists(cat)):
                if (self.question_set_manager.groupExists(group, cat)):
                    self.question_set_manager.addQuestion(cat, group, question)
            else:
                raise CantAddQuestion
        except CantAddQuestion as e:
            logger.error("Failed to add prompt")
        else:
            logger.info("Prompt added")

    def add_new_group(self, cat, group):
        # category musy be provided
        try:
            if (self.question_set_manager.groupExists(group, cat)):
                raise CantAddGroup
            else:
                self.question_set_manager.addGroup(group, cat)
        except CantAddGroup as e:
            logger.error("can't add group")
        else:
            logger.info("Group Added")


    def add_new_category(self, cat):
        try:
            if (self.question_set_manager.categoryExists(cat)):
                raise CantAddCategory
            else:
                self.question_set_manager.addCategory(cat)
        except CantAddCategory as e:
            logger.error("can't add category")
        else:
            logger.info("Category added")

    def confirm_changes(self):
        self.question_set_manager.confirm_changes()
        self.promptObjects = []     # reset volatile prompts

    ## ACQUIRE QUESTIONS ##

    def constructPromptObjects(self):
        self.promptObjects.append(self.converter.convertAllObjects())

    def constructCategoryObjects(self, cat):
        logger.info("Adding category prompts: " + str(cat))
            
        all_cat_prompts = self.converter.convertCategoryObjects(cat)
        if (all_cat_prompts == []):
            raise CategoryEmptyPrompts
        else:
            self.promptObjects.append(self.converter.convertCategoryObjects(cat))

    def category_exists(self, category):
        return self.question_set_manager.categoryExists(category)

    def category_added(self, category):
        try:
            logger.error(self.promptObjects)
            if self.promptObjects == []:
                return False
            for cat in self.promptObjects:
                logger.error("CATS")
                logger.error(cat)
                if (cat[0].getCategory() == category):
                    logger.info('Category already added')
                    raise CategoryAlreadyAdded
                    break
            logger.info('Category not added to volatile prompts')
        except CategoryAlreadyAdded as e:
            logger.error("Category already added")
            raise CategoryAlreadyAdded

    def clearPromptObjects(self):
        self.promptObjects = []
        logger.info('Prompts cleared')

    def constructQuestionObjects_group(self, cat, group):
        pass

    def get_all_categories(self):
        all_data = self.question_set_manager.AccessDataSet.data
        all_categories = []
        for cat in all_data["category"]:
            all_categories.append(cat)
        return all_categories

    def get_all_groups(self, cat):
        all_data = self.question_set_manager.AccessDataSet.data
        all_groups = []
        for grp in all_data["category"][cat]:
            all_groups.append(grp)

        return all_groups

    def get_all_hard_prompts(self):
        data = self.question_set_manager.AccessDataSet.data

        return data

    ## ---------------- ##

    ## MODULE INITIATORS ##
	
    """
        QUESTIONNAIRE functions
    """

    def init_questionnaire(self, finished_func):
        self.ques = Questionnaire(finished_func)
        logger.info('Initiating Questionnaire')

    def start_questionnaire(self, view_type, master):
        self.ques.init_prompts(self.promptObjects)
        self.ques.start_questionnaire(view_type, master)

    def finished_questionnaire(self):
        logger.info("Questionnaire finished")
        self.gui_mang.update_volatile_prompts()

        #convert data
        self.produce_summary()

        #Store graph
        self.pass_summary_data_graph()
        self.produce_graph()
		
    """
        GRAPH functions
    """
    def init_graph_module(self):
        self.graph_module = Graph()
        logger.info("Report module initialized")

    def pass_summary_data_graph(self):
        if (self.cat_summ == {}):
            logger.debug("Summary data is empty")
        self.graph_module.convert_data(self.cat_summ)

    def produce_graph(self):
        self.graph_module.create_chart()
        
        graph_name = "summary_graph"
        
        self.graph_module.store_fig('png', graph_name)

    def show_graph(self):
        self.graph_module.show_graph()

    """
        REPORT functions
    """
		
    def init_report_module(self):
        self.report_module = Report(self.promptObjects, 'win')
        
        title = self.project_info.get_title_project()
        file_name = title + "Report.pdf"
        
        self.report_module.set_filename(file_name)
	
    def produce_report(self):
        # Pass Project Infos
        self.report_module.set_info(self.project_info.struct_data())
        self.report_module.start_win_gen()
        logger.error("Report created")

    """
        Produce a summary structure to help produce graphs
        {
            cat0: [(g0,(n, m)), (g1,(n, m))],
            cat1: [(g0,(n,m)), (g1, (n, m))],
            ...

        }
    """
    def produce_summary(self):
        self.cat_summ = {}
        for cat in self.promptObjects:  # Get category names
            cat_name = cat[0].getCategory() # Get first QUestion object and get its category
            groups = {} # temporary storage for groups
            for q in cat:   # access prompts for each category
                logger.info("Question: " + str(q.getStatus()))
                if (q.getGroup() in groups):    # Update group
                    if (q.getStatus() == 'y'):
                        groups[q.getGroup()] = (groups[q.getGroup()][0] + 1, groups[q.getGroup()][1])
                    else:
                        groups[q.getGroup()] = (groups[q.getGroup()][0], groups[q.getGroup()][1] + 1)
                else:   # Create group
                    groups[q.getGroup()] = (0,0)
                    logger.info("New group created: " + str(groups))
                    logger.info("Status : " + str(q.getStatus()))
                    if (q.getStatus() == 'y'):
                        groups[q.getGroup()] = (groups[q.getGroup()][0] + 1, groups[q.getGroup()][1])
                    else:
                        logger.info(groups[q.getGroup()][1])
                        groups[q.getGroup()] = (groups[q.getGroup()][0], groups[q.getGroup()][1] + 1)

            group_tuple = []
            # store groups in (group, (yes, no)) in a list of groups
            for group in groups:
                logger.info("Adding group summary: " + str(group))
                summary = (group, (groups[group][0], groups[group][1]))
                logger.info(summary)
                group_tuple.append(summary)

            # append list of groups in dictionary
            self.cat_summ[cat_name] = group_tuple
        logger.error("Finished creating summary")


    ## --------------- ##
    ## DEBUG FUNCTIONS ##

    def debug_displayPrompts(self):
        for cat in self.promptObjects:
            for q in cat:
                self.debug_displayIdvPrompt(q)

    def debug_displayIdvPrompt(self, quesObj):
        # Essential entries
        print(quesObj.getPrompt())
        print(quesObj.getStatus())
        print(quesObj.getCategory())
        print(quesObj.getGroup())

        # Additional entries
        print(quesObj.getComment())

    def debug_display_summary(self):
        print(self.cat_summ)



class ProjectManager(GeneralManager):
    pass
