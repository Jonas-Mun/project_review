from abc import ABC, abstractmethod

"""
    Provides the basic functions required by the Manager controller
    to manipulate the Gui view.

    func_dict -> Access to all function calls in dictionary format.
                 passed by controller
"""

class BaseGui(ABC):
    def __init__(self, func, container):
        pass

    @abstractmethod
    def start_gui(self):
        pass

    @abstractmethod
    def clear_hard_prompts(self):
        pass

    @abstractmethod
    def update_hard_prompts(self):
        pass

    @abstractmethod
    def clear_volatile_prompt_display(self):
        pass

    @abstractmethod
    def update_volatile_prompt_display(self, p_string):
        pass

    @abstractmethod
    def set_title(self, title):
        pass

    @abstractmethod
    def error_display(self, msg):
        pass
