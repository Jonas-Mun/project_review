from tkinter import *
from tkinter import ttk

# Drop list with update values function
class DropList(Frame):
    def __init__(self, parent, item_list, name):
        Frame.__init__(self, parent)
        self.name = name
        self.combo_box = ttk.Combobox(self, values=item_list)
        self.combo_box.current(0)
        self.combo_box.pack(side=LEFT)

            # expose some combobox methods as methods on this object
        self.current = self.combo_box.current
        self.get = self.combo_box.get
        self.bind = self.combo_box.bind

    def update_values(self, value):
        self.combo_box['values'] = value
        self.combo_box.current(0)


# Multiple Drop lists with entry
class MultiDropEntry(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent)
        self.drop_lists = {}
        # Create DropList components
        for i in args:
                drop_list = DropList(self, i[1], i[0])
                drop_list.pack(side=LEFT)
                self.drop_lists[i[0]] = drop_list

        self.entry_text = Entry(self)
        self.entry_text.pack(side=LEFT)

        self.get_entry = self.entry_text.get()


    def update_spec_list(self, name, new_vals):
        self.drop_lists[name].update_values(new_vals)

# Multiple Drop Lists with entry under label
class MultiDropEntryLabelled(MultiDropEntry):
    def __init__(self, parent, event, label,*args, **kwargs):
        self.label = LabelFrame(parent, text=label)
        self.label.pack()
        MultiDropEntry.__init__(self, self.label, *args, **kwargs)
        self.button = Button(self.label, text="Add", command=self.activate_event)
        self.button.pack(side=RIGHT)

        self.event = event

    def activate_event(self):
        current_drop_list_selected = []
        for name in self.drop_lists:
            print(name)
            drop_list = self.drop_lists[name]

            current_drop_list_selected.append(drop_list.get())
        print(self.entry_text.get())
        current_drop_list_selected.append(self.entry_text.get())

        print(current_drop_list_selected)
        self.event(current_drop_list_selected)

    def get_container(self):
        return self.label


class DisplayPrompts(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent)
        self.scroll_bar_y = Scrollbar(self, orient="vertical")
        self.scroll_bar_y.pack(side = RIGHT, fill = Y)
        
        self.scroll_bar_x = Scrollbar(self, orient="horizontal")
        self.scroll_bar_x.pack(side=BOTTOM, fill = X)
        
        self.list_box = Listbox(self, yscrollcommand = self.scroll_bar_y.set, xscrollcommand = self.scroll_bar_x.set)
        self.list_box.pack()

        self.insert = self.list_box.insert
        self.list_box["width"] = 50

    def update_list_vals(self, values):
        for i in values:	#add elements
            self.list_box.insert(END, i)
            self.insert = self.list_box.insert
            self.delete = self.list_box.delete
    
    def update_list_box(self, value):
        self.list_box.insert(END, value)

    def clear_list_box(self):
        self.list_box.delete(0,END)

class LabelEntry(Frame):
    def __init__(self, parent, label):
        Frame.__init__(self, parent)
        self.label = Label(self, text=label)
        self.label.pack(side=LEFT)
        self.entry = Entry(self)
        self.entry.pack(side=LEFT)
    
        self.get = self.entry.get
        
def add_stuff(master):
	add_prompt_drops = MultiDropEntryLabelled(master, '', "Add Question",("category", ([1,2,3,4])), ("group", [6,7,8]))
	add_prompt_drops.pack()
	print(add_prompt_drops.drop_lists)
	
	add_group_drop = MultiDropEntryLabelled(master, '',"Add Group",("category",([1,2,3,4])))
	add_group_drop.pack()
	
	add_category_drops = MultiDropEntryLabelled(master, '',"Add Category")
	add_category_drops.pack()
		
