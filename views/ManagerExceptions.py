class CategoryAlreadyAdded(Exception):
    pass

class CategoryNotAdded(Exception):
    pass

class CategoryDoesNotExist(Exception):
    pass

class CantAddQuestion(Exception):
    pass

class CantAddCategory(Exception):
    pass

class CantAddGroup(Exception):
    pass
    
class CategoryEmptyPrompts(Exception):
    pass


