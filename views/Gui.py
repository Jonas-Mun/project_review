from tkinter import *
from tkinter import ttk
from tkinter import messagebox

from abc import ABC, abstractmethod

from .BaseGui import BaseGui

from .widgets.custom_widgets import MultiDropEntryLabelled
from .widgets.custom_widgets import DropList
from .widgets.custom_widgets import DisplayPrompts
from .widgets.custom_widgets import LabelEntry

DEBUG = False

class GuiTkint(BaseGui):

    def __init__(self, funcs, master):
        self.master = master
        self.func_dict = funcs

    def start_gui(self):
        self.main_frame = Frame(self.master, pady=50)
        
        if (DEBUG):
            self.main_frame["bg"] = "brown"
        
        self.main_frame.pack_propagate(0)
        self.main_frame.pack()

        self.project_info_component()
        self.project_info_display(self.main_frame,  0, 1)
        self.specify_question_cmp(self.main_frame, 1, 0)
        #self.state_component(self.main_frame)
        self.volatile_prompt_display_component(self.main_frame, 1, 1)
        self.module_components(self.main_frame, 2, 0)
        """
        self.test_button = Button(self.master, text="Start Questionnaire", command=self.ques_func)
        self.test_button.pack()

        self.option_menu(['test', 'Oil', 'out'])

        """
        self.master.mainloop()

    ##STAGE 1 - Beginning stage ##
    def beg_stage(self):
        pass

    ##STAGE 2 - Main stage ##
    def main_stage(self):
        self.main_frame = Frame(self.master, pady=50)

        if (DEBUG):
            self.main_frame["bg"] = "brown"

        self.main_frame.pack_propagate(0)
        self.main_frame.pack()

        #self.project_info_entry(self.main_frame, 0, 1)
        self.project_info_display(self.main_frame,  0, 1)
        self.specify_question_cmp(self.main_frame, 1, 0)
        #self.state_component(self.main_frame)
        self.volatile_prompt_display_component(self.main_frame, 1, 1)
        self.module_components(self.main_frame, 2, 0)
        """
        self.test_button = Button(self.master, text="Start Questionnaire", command=self.ques_func)
        self.test_button.pack()

        self.option_menu(['test', 'Oil', 'out'])

        """
        self.master.mainloop()

    ##STAGE 3 - Final stage ##
    def final_stage(self):
        pass

    ### Component Functions ###
    """
        Entry for current project being reviewed
    """

    def project_info_entry(self, container, row_index, column_index):
        self.project_title_frame = Frame(container)
        self.project_title_frame.grid(row=row_index, column=column_index)

        self.project_title_label = Label(self.project_title_frame, text="Project Name:")
        self.project_title_label.pack(side=LEFT)

        self.project_title_entry = Entry(self.project_title_frame)
        self.project_title_entry.pack(side=LEFT)

        self.project_title_button = Button(self.project_title_frame, text="Enter", command=lambda : self.func_dict["set_project_title"](self.project_title_entry.get()))
        self.project_title_button.pack(side=LEFT)
    """
        Display info of current project
    """

    def project_info_display(self, container, row_index, column_index):
        
        info_data = self.func_dict["get_all_project_info"]()
        
        
            
        
        self.project_info_display_frame = Frame(container)
        self.project_info_display_frame.grid(row=row_index, column=column_index+1)

        # Project Title
        self.project_title_info_label = Label(self.project_info_display_frame, text=info_data[0][0])
        self.project_title_info_label.grid(row=0, column=0)
        self.project_title_info = Label(self.project_info_display_frame, text=info_data[0][1])
        self.project_title_info.grid(row=0, column=1)
        
        # Date 
        self.date_info_label = Label(self.project_info_display_frame, text=info_data[1][0])
        self.date_info_label.grid(row=1, column=0)
        self.date_info = Label(self.project_info_display_frame, text=info_data[1][1])
        self.date_info.grid(row=1, column=1)
        
        self.check_info_button = Button(self.project_info_display_frame, text="Show More Info", command=self.project_info_component)
        self.check_info_button.grid(row=row_index+2, column=column_index)
        
        

    """
        Select questions based on category to answer for current question
    """
    def specify_question_cmp(self, container, row_index, column_index):
        # Drop list frame
        
        self.frame_label = LabelFrame(container, text="Add Current Questions")
        self.frame_label.grid(row=row_index, column=column_index)
        
        self.drop_list_frame = Frame(self.frame_label, width=200, height=100)
        
        if (DEBUG):
            self.drop_list_frame["bg"] = "blue"
            
        self.drop_list_frame.grid(row=row_index, column=column_index)

        title = Label(self.drop_list_frame, text="Category")
        title.grid(row=0,column=0, padx=8)

        ## 1,1 - categories drop list
        
        all_categories = self.func_dict["get_all_categories"]()

        self.cat_drop_list = DropList(self.drop_list_frame, all_categories, "category")
        self.cat_drop_list.grid(row=0, column=1)

        ## 2 - Buttons
        self.button_frame = Frame(self.drop_list_frame)
        self.button_frame.grid(row=0, column=2, rowspan=2, padx=8)

        self.add_button = Button(self.button_frame, text="Add", command=self.update_categories)
        self.add_button.pack(pady=2)

        self.clear_button = Button(self.button_frame, text="Clear", command=self.func_dict['clear_volatile_prompts'])
        self.clear_button.pack(pady=2)

        self.edit_prompts_button = Button(self.drop_list_frame, text="Add New Questions", command=self.edit_prompts_component)
        self.edit_prompts_button.grid(row=row_index+1, column=column_index)


    def state_component(self, container):
        self.state_frame = Frame(container, padx=8)
        
        if (DEBUG):
            self.state_frame["bg"] = "pink"
        
        self.state_frame.grid(row=0, column=1, padx=40)

        self.state_text = StringVar()
        self.state_text.set("Development")

        self.state_label = Label(self.state_frame, textvariable=self.state_text)
        self.state_label.grid(row=0, column=0)

    def volatile_prompt_display_component(self, container, row_index, column_index):
        prompts = []

        self.volatile_prompt_label = LabelFrame(container, text="Current Questions")
        self.volatile_prompt_label.grid(row=row_index, column=column_index, columnspan=3, rowspan=100, padx=25)
        
        self.volatile_prompt_display = DisplayPrompts(self.volatile_prompt_label, prompts)
        #self.volatile_prompt_display.width = 200
        self.volatile_prompt_display.grid(row=row_index, column=column_index, columnspan=3, rowspan=100)

    def module_components(self, container, row_index, column_index):
    
        self.frame_label_modules = LabelFrame(container, text="Modules")
        self.frame_label_modules.grid(row=row_index, column=column_index, pady=20)
    
        self.module_frame = Frame(self.frame_label_modules, bg="green",width=100, height=100)
        self.module_frame.grid(row=row_index, column=column_index, pady=40)
        
        if (DEBUG):
            self.module_frame["bg"] = "green"

        self.quest_frame = Frame(self.module_frame, bg="pink")
        self.quest_frame.grid(row=0, column=0, padx=15)
        self.quest_button = Button(self.quest_frame, text="Start Review", command=self.func_dict['questionnaire'])
        self.quest_button.pack()

        self.report_frame = Frame(self.module_frame)
        self.report_frame.grid(row=0, column=1, padx=15)
        self.report_button = Button(self.report_frame, text="Make Report", padx=10, command=self.func_dict["make_report"])
        self.report_button.pack()
        
        self.graph_frame = Frame(self.module_frame)
        self.graph_frame.grid(row=1, column=1, pady=20)
        self.graph_button = Button(self.graph_frame, text="Show Graph", command=self.func_dict["show_graph"])
        self.graph_button.pack()

        
    def project_info_component(self ):
        self.general_info_window = Toplevel(self.master)
    
        self.general_info_frame = Frame(self.general_info_window)
        self.general_info_frame.pack()
        
        # Project Title entry
        self.title_entry_label = LabelEntry(self.general_info_frame, "Project:")
        self.title_entry_label.pack()
        
        # Date Entry
        self.date_entry_label = LabelEntry(self.general_info_frame, "Date:")
        self.date_entry_label.pack()
        
        # Locaion Entry
        self.location_entry_label = LabelEntry(self.general_info_frame, "Location:")
        self.location_entry_label.pack()
        
        # Customer Entry
        self.customer_entry_label = LabelEntry(self.general_info_frame, "Customer:")
        self.customer_entry_label.pack()
        
        # Job Type Entry
        self.job_type_entry_label = LabelEntry(self.general_info_frame, "Job Type:")
        self.job_type_entry_label.pack()
        
        # Well Name
        self.well_name_entry_label = LabelEntry(self.general_info_frame, "Well Name:")
        self.well_name_entry_label.pack()
        
        # Version entry
        self.version_entry_label = LabelEntry(self.general_info_frame, "Version:")
        self.version_entry_label.pack()
        
        # ENgineer entry
        self.engineer_entry_label = LabelEntry(self.general_info_frame, "SLB Engineer:")
        self.engineer_entry_label.pack()
        
    """
        Add categories,groups, and questions to 'Hard Prompts'
    """


    def edit_prompts_component(self):
        self.edit_prompts_window = Toplevel(self.master)

        self.adding_new_prompts_frame = Frame(self.edit_prompts_window)
        self.adding_new_prompts_frame.grid(row=0, column=0, columnspan=3)

        """
            Question Entry
        """
        self.add_question_entry = MultiDropEntryLabelled(self.adding_new_prompts_frame, self.func_dict["add_new_question"], "Add Question", ("category", (self.func_dict["get_all_categories"]())), ('group', ([''])) )
        # update groups when category changes
        self.add_question_entry.drop_lists["category"].bind("<<ComboboxSelected>>", lambda event:self.add_question_entry.drop_lists["group"].update_values(self.func_dict["get_all_groups"](self.add_question_entry.drop_lists["category"].get())))

        self.add_question_entry.pack()

        """
            Group Entry
        """

        self.add_grp_entry = MultiDropEntryLabelled(self.adding_new_prompts_frame, self.func_dict["add_new_group"], "Add Group", ("category", (self.func_dict["get_all_categories"]())))
        self.add_grp_entry.pack()

        """
            Category Entry
        """

        self.add_category = MultiDropEntryLabelled(self.adding_new_prompts_frame, self.func_dict["add_new_category"], "Add Category")
        self.add_category.pack()

        """
            All Hard Prompts
        """

        self.display_prompts_info_frame = Frame(self.edit_prompts_window)
        self.display_prompts_info_frame.grid(row=0, column=4)
        self.hard_prompts_display = DisplayPrompts(self.display_prompts_info_frame)
        self.update_hard_prompts()
        self.hard_prompts_display.pack()

        """
            Confirm Changes
        """

        self.confirm_button = Button(self.edit_prompts_window, text="Confirm and Exit", command=self.confirm_hard_changes)
        self.confirm_button.grid(row=1, column=2)

    # --------------
    def update_hard_prompts(self):
        data = self.func_dict["get_all_hard_prompts"]()
        for cat in data["category"]:
            self.hard_prompts_display.insert(END, cat)
            for grp in data["category"][cat]:
                self.hard_prompts_display.insert(END,"    " + grp)
                for ques in data["category"][cat][grp]["questions"]:
                    question = ques["question"]
                    self.hard_prompts_display.insert(END,"        " + question)
                    
    def clear_hard_prompts(self):
        self.hard_prompts_display.clear_list_box()


    ## Functionailities
    
    def set_title(self, title):
        self.title_project["text"] = title
    
    def clear_volatile_prompt_display(self):
        self.volatile_prompt_display.clear_list_box()
        
    def update_volatile_prompt_display(self, p_string):
        self.volatile_prompt_display.update_list_box(p_string)
    
    def confirm_hard_changes(self):
        self.func_dict["confirm_changes"]
        self.edit_prompts_window.destroy()

    def update_categories(self):
        self.func_dict['add_volatile_prompts'](self.cat_drop_list.get())

    def clear_prompt_list(self):
        self.volatile_display_prompt.delete(0, END)

    def error_display(self, msg):
        messagebox.showerror('error', msg)

    def get_selected_option(self):
        self.option_variable.get()

    def list_box(self, items):
        pass



