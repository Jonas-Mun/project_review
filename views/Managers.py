from tkinter import *

import logging
from .ManagerExceptions import *

from .Gui import GuiTkint

view_manager_logger = logging.getLogger('manager_application.view')

class ManagerView:

    def __init__(self, manager, view_type):
        self.main_manager = manager
        self.view_type = view_type
        self.logger = logging.getLogger('manager_application.view.ManagerView')
        self.logger.info('creating an instance of ManagerView')

    def start_view(self):
        if (self.view_type == "debug"):
            self.logger.debug('debugging view')
            
        else:
            self.gui_tkinter()

    ## Views ##
    def gui_tkinter(self):
        self.logger.info("initializing gui_tkinter")
        self.root = Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.close_view)
        self.root.geometry("700x500")
        self.gui_vew = GuiTkint(self.main_function_caller(), self.root)
        self.gui_vew.start_gui()

    def close_view(self):
        print("DELETING")
        self.main_manager.graph_module.close_graph()    # close plt
        self.root.destroy()

    ## Functions to pass to views ##

    def main_function_caller(self):
        # call questionnaire
        # add volatile prompts
        # add prompts
        # add category
        # add group

        func_dict = {
                'questionnaire': self.call_questionnaire,
                'add_volatile_prompts': self.add_volatile_prompts,
                'get_volatile_prompts': self.get_volatile_prompts,
                'clear_volatile_prompts': self.clear_volatile_prompts,
                'update_volatile_prompts': self.update_volatile_prompts,
                'add_new_question': self.add_new_question,
                'add_new_category': self.add_new_category,
                'add_new_group': self.add_new_group,
                'confirm_changes': self.main_manager.confirm_changes,
                'get_all_categories': self.main_manager.get_all_categories,
                'get_all_groups': self.main_manager.get_all_groups,
                'get_all_hard_prompts': self.main_manager.get_all_hard_prompts,
                'set_project_title': self.set_project_title,
                'set_project_date': self.main_manager.project_info.set_date,
                'get_project_title': self.main_manager.project_info.get_title_project,
                'get_project_date': self.main_manager.project_info.get_date,
                'get_all_project_info': self.main_manager.project_info.struct_data,
				'make_report': self.main_manager.produce_report,
                'show_graph': self.main_manager.show_graph,
            }

        return func_dict

    def set_project_title(self, title):
        self.main_manager.project_info.set_title_project(title)
        self.gui_vew.set_title(title)

    def add_new_question(self, list_info):
        try:
            self.main_manager.add_new_prompt(list_info[0], list_info[1], list_info[2])
        except CantAddQuestion as e:
            self.gui_vew.error_display("Can't add question")
            self.logger.error("Can't add question")
        else:
            self.gui_vew.clear_hard_prompts()
            self.gui_vew.update_hard_prompts()

    def add_new_category(self, list_info):
        try:
            self.main_manager.add_new_category(list_info[0])
        except CantAddCategory as e:
            self.gui_vew.error_display("Can't add category")
            self.logger.error("Can't add category")
        else:
            self.gui_vew.clear_hard_prompts()
            self.gui_vew.update_hard_prompts()


    def add_new_group(self, list_info):
        try:
            self.main_manager.add_new_group(list_info[0], list_info[1])
        except CantAddGroup as e:
            self.gui_vew.error_display("Can't add group")
            self.logger.error("Can't add group")
        else:
            self.gui_vew.clear_hard_prompts()
            self.gui_vew.update_hard_prompts()

    def call_questionnaire(self):
        self.logger.info("calling questionnaire")
        if (self.root == None):
            self.logger.debug("No root for gui")
            return False
        elif (self.main_manager.promptObjects == []):
            self.logger.error("Can't start questionnaire: no prompts to display")
            self.gui_vew.error_display("Can't start questionnaire: No Questions added")
        else:
            self.logger.info("creating questionnaire")
            self.main_manager.start_questionnaire(self.view_type, self.root)
            

    # Select questions to add
    def add_volatile_prompts(self, cat):

        try:
            self.main_manager.category_added(cat)   # Check if catery has been added

            if (self.main_manager.category_exists(cat)):
                self.main_manager.constructCategoryObjects(cat)
                self.update_volatile_prompts()
            else:
                raise CategoryDoesNotExist

        except CategoryAlreadyAdded as e:
            self.gui_vew.error_display("Category already added")
            self.logger.error("Category already added")
        except CategoryDoesNotExist as e:
            self.gui_vew.error_display("Category does not exist")
            self.logger.error("Category does not exist")
        except CategoryEmptyPrompts as e:
            self.logger.error("Category has empty prompts")

    def get_volatile_prompts(self):
        return self.main_manager.promptObjects

    def clear_volatile_prompts(self):
        self.gui_vew.error_display("Function not added")
        self.logger.error("Clear volatile prompt not added")
        pass

    def update_volatile_prompts(self):
       index = 0
       self.gui_vew.clear_volatile_prompt_display()# Reset display box
       for cat in self.main_manager.promptObjects:
           for prompt in cat:
               prompt_string = str(index) + ' - ' + prompt.getCategory() + ' - ' + prompt.getGroup() + ' - ' + prompt.getPrompt() + ' - '+ prompt.getStatus()
               self.gui_vew.update_volatile_prompt_display(prompt_string)
               index += 1
