from modelData.Question import Question
from modules.Report.Report import Report

qone = Question('Video games', 'FPS', 'Halo')
qone.updateComment("IT is the best thing I have ever played!")
qtwo = Question('Video games', 'FPS', 'CoD')
qthree = Question('Books', 'Fiction', 'Ancient City')
qfour = Question('Books', 'Fiction', 'URRAAAAHHH')
qfive = Question('Books', 'History', 'The Ancient City')
qsix = Question('Books', 'History', 'Napoleon the Great')
qseven = Question('Books', 'History', 'The Empire of the Sea')
qeight = Question('Books', 'Fiction', 'The Last Wish')
qnine = Question('Books', 'Fiction', 'The Sword of Destiny')
qten = Question('Books', 'Fiction', 'Flipped')

qeleven = Question('Video games', 'FPS', 'TitanFall')
qtwelve = Question('Video games', 'FPS', 'Battlefield')
qthirteen = Question('Video games', 'FPS', 'Counter-Strike')
qfourteen = Question('Video games', 'Open world', 'The Witcher 3')  #Next page
qfifteen = Question('Video games', 'FPS', 'TitanFall')
qsixteen = Question('Video games', 'FPS', 'TitanFall')
qseventeen = Question('Video games', 'FPS', 'TitanFall')
qeighteen = Question('Video games', 'FPS', 'TitanFall')
qnineteen = Question('Video games', 'FPS', 'TitanFall')
qtwenty = Question('Video games', 'FPS', 'TitanFall')
qtwentyone = Question('Video games', 'FPS', 'TitanFall')


raw_data = [[qone,qtwo,qeleven,qtwelve,qthirteen,qfourteen,qfifteen,qsixteen,qseventeen,qeighteen,qnineteen,qtwenty,qtwentyone],[qthree,qfour,qfive,qsix,qseven,qeight,qnine,qten]]

test_report = Report(raw_data, 'win')
test_report.set_filename('test_pdf.pdf')
test_report.start_win_gen()
