import json

"""
data_template = {
        "type": "__question__",
        "category": {
            "Oil": {
                            "Header": {
                                'questions': [{q_id:z, ques: 'Stuff'}, 'Stuff']},
                    },
            "Well": {
                        {
                            "Header": {
                                'questions': [{q_id:z, ques: 'Stuff'}],
                                }
                        }
            },
        }
    }

json_fata = json.dumps(data_template, indent=2)

with open('data.json', 'w') as outfile:
    json.dump(data_template, outfile, indent=2)

"""
class CreateSet():
    """
        Create a set of categories of a type containing Header and Questions
    """
    def __init__(self, file, promptType):
        self.file = file
        self.promptType = promptType
        self.data = {}

    def getSetData(self):
        with open(self.file) as infile:
            self.data = json.load(infile)

        return self.data

    def initNewSet(self):
        data_template = {
                    "type": self.promptType,
                    "category": {},
                }
        self.data = data_template

    """
        Create Data
    """
    def createCategory(self, cat):
        data = { cat: {} }
        self.data["category"].update(data)

    def createGroup(self, group, cat):
        groupSet = {group: {'next_id':1, 'questions':[]}}
        self.data["category"][cat].update(groupSet)

    def createQuestion(self,ques,group,cat):
        next_id = self.data["category"][cat][group]["next_id"]
        question = { 'q_id': next_id, 'question': ques}
        self.data["category"][cat][group]["next_id"] += 1
        self.data["category"][cat][group]['questions'].append(question)

    """
        Remove Data
    """
    def removeCategory(self, cat):
        if self.categoryExists(cat):
            del self.data["category"][cat]
            return True
        else:
            return False

    def removeGroup(self, grp, cat):
        if self.groupExists(grp, cat):
            del self.data["category"][cat][grp]
            return True
        else:
            print("Key Error: {e}".format(e))
            return False

    def removeQuestion(self, ques, grp, cat):
        questions = self.data["category"][cat][grp]["questions"]

        ques_list = []

        # TODO: Refactor to own function
        for i in range(0, len(questions)):
            question = questions[i]["question"]
            if ques == question:
                ques_list.extend(questions[i+1:len(questions)])
                self.data["category"][cat][grp]["questions"] = ques_list
                return True
            else:
                ques_list.extend(questions[i])

        self.data["category"][cat][grp]["questions"] = ques_list

        return False

    def storeData(self, outputFile):
        with open(outputFile, 'w') as outfile:
            json.dump(self.data, outfile, indent=2)

    """
        Data existence checkers
    """
    def categoryExists(self,cat):
        if cat in self.data['category']:
            return True
        else:
            return False

    def groupExists(self, grp, cat):
        print("group " + grp)
        print("category" + cat)
        if (grp in self.data['category'][cat]):
            return True
        else:
            return False
