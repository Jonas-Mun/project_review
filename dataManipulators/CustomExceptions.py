class CantAddQuestion(Exception):
    pass

class CantRemoveQuestion(Exception):
    pass

class CategoryExists(Exception):
    pass

class CategoryDoesNotExist(Exception):
    pass

class GroupExists(Exception):
    pass

class GroupDoesNotExist(Exception):
    pass
