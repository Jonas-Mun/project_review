import json
import logging

from .createSet import CreateSet
from .CustomExceptions import *

manage_questions = logging.getLogger('manager_application.data_manp.manage_questions')

class ManageSetQuestions():

    def __init__(self, quesFile, promptType):
        self.logger = logging.getLogger('manager_application.data_manp.manage_questions.ManageSetQuestions')
        self.quesFile = quesFile
        self.AccessDataSet = CreateSet(quesFile, promptType)
        self.initQuestions()

        self.logger.info("created ManageSetQuestions instance")


    def initQuestions(self):
        self.logger.info("acquiring prompts from data file")
        self.AccessDataSet.getSetData()
        if self.AccessDataSet.data == {}:
            self.AccessDataSet.initNewData()
            self.logger.info("empty file - creating new set of data")

    def addQuestion(self, cat, group, ques):
        self.logger.info("Category: " + str(cat) + '\n' + "Group: " + str(group) + '\n' + "Question: " + str(ques))
        try:
            if self.canAddQuestion(cat,group,ques):
                self.AccessDataSet.createQuestion(ques, group, cat)
            else:
                self.logger.error("can't add question")
                raise CantAddQuestion
        except CantAddQuestion:
            self.logger.error("Can't add Question")

    def remvQuestion(self, ques, grp, cat):
        isSuccess = self.AccessDataSet.removeQuestion(ques, grp, cat)

        try:
            if isSuccess:
                self.logger.info("removed question: " + str(ques))
            else:
                self.logger.error("failed to remove question: " + str(ques))
                raise CantRemoveQuestion
        except CantRemoveQuestion as e:
            self.logger.error("Can't remove question")

    def addCategory(self, cat):
        try:
            if self.AccessDataSet.categoryExists(cat):
                self.logger.info("category already exists: " + str(cat))
                raise CategoryExists
            else:
                self.logger.info("adding category: " + str(cat))
                self.AccessDataSet.createCategory(cat)
        except CategoryExists as e:
            self.logger.error("Category already exists")

    def remvCategory(self, cat):
        try:
            if self.AccessDataSet.categoryExists(cat):
                self.AccessData.removeCategory(cat)
                self.logger.info("category remove: " + str(cat))
            else:
                # Create awareness
                raise CategoryDoesNotExist
                self.logger.error("can not remove category - does not exist: " + str(cat))
        except CategoryDoesNotExist as e:
            self.logger.error("Category does not exist")

    def categoryExists(self, cat):
        if self.AccessDataSet.categoryExists(cat):
            self.logger.info("category exists: " + str(cat))
            return True
        else:
            self.logger.info("category does not exist: " + str(cat))
            return False

    def addGroup(self, grp, cat):
        try:
            if self.AccessDataSet.groupExists(grp, cat):
                self.logger.error("group already exists: " + str(grp))
                raise GroupExists
            else:
                self.AccessDataSet.createGroup(grp, cat)
                self.logger.info("group added: " + str(cat))
                return True
        except GroupExists as e:
            self.logger.error("group already exists")

    def remvGroup(self, grp, cat):
        try:
            if self.AccessDataSet.groupExists(grp, cat):
                self.AccessDataSet.removeGroup(grp, cat)
            else:
                self.logger.error("group does not exists: " + str(cat))
                raise GroupDoesNotExist
        except GroupDoesNotExist as e:
            self.logger.error("Group does not exist")

    def groupExists(self, grp, cat):
        if self.AccessDataSet.groupExists(grp, cat):
            return True
        else:
            return False

    def canAddQuestion(self,cat, grp,ques):
        try:
            if self.AccessDataSet.categoryExists(cat):
                if self.AccessDataSet.groupExists(grp, cat):
                    self.logger.info("question can be added: " + str(ques) + " to " + str(grp))
                else:
                    print("Group does not exist")
                    self.logger.error("can't add question: " + str(ques) + " - group does not exists -> " + str(grp))
                    raise GroupDoesNotExist
            else:
                print("Category does not exist")
                self.logger.error("can't add question: " + str(ques) + " - category does not exist -> " + str(cat))
                raise CategoryDoesNotExist
        except GroupDoesNotExist as e:
            self.logger.error("Group does not exist")
        except CategoryDoesNotExist as e:
            self.logger.error("Category does not exist")


    def confirm_changes(self):
        self.AccessDataSet.storeData(self.quesFile)
