import unittest
import json

from createSet import CreateSet

class TestCreateSet(unittest.TestCase):

    def setUp(self):
        self.data_template = {
                "type": "__test_question__",
                "category": {}
                }

    def test_initNewSet(self):
        accessSet = CreateSet('./data/test/test.json', '__test_question__')

        json_cmp = {
                "type": "__test_question__",
                "category": {}
                }

        accessSet.initNewSet();
        self.assertEqual(accessSet.data, json_cmp)

    def test_getSetData(self):
        accessSet = CreateSet('./data/test/test_template.json', '__test_question__')

        json_cmp = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {
                            "questions": []
                            }
                        },
                    "Well": {
                        "Footer": {
                            "questions": []
                            }
                        }
                    }
                }
        accessSet.getSetData()
        self.assertEqual(accessSet.data, json_cmp)

    def test_createCategory(self):
        accessSet = CreateSet('./data/test/test_category_v1.json', '__test_question__')

        json_cmp = {
                "type": "__test_question__",
                "category": {
                    "Oil": {}
                    }
                }

        accessSet.data = self.data_template
        accessSet.createCategory("Oil")
        self.assertEqual(accessSet.data, json_cmp)

    def test_createGroup(self):
        accessSet = CreateSet('./data/test/test_template.json','__test_question__')
        data = {
                "type": "__test_question__",
                "category": {
                    "Oil": {}
                    }
                }

        accessSet.data = data

        json_cmp = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {'next_id': 1, 'questions': []}
                        }
                    }
                }
        accessSet.createGroup("Header", "Oil")
        self.assertEqual(accessSet.data, json_cmp)

    def test_createQuestion(self):
        accessSet = CreateSet('./data/test/test_template.json', '__test_question__')
        data = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {"next_id": 1, "questions": []}
                        }
                    }
                }
        accessSet.data = data

        json_cmp = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {"next_id": 2, "questions": [{"q_id":1, "question": "Cover Page"}]}
                        }
                    }
                }

        accessSet.createQuestion("Cover Page", "Header", "Oil")
        self.assertEqual(accessSet.data, json_cmp)

    def test_removeCategory(self):
        accessSet = CreateSet('./data/test/test.json', '__test_question__')
        accessSet_2 = CreateSet('./data/test/test.json', '__test_question__')
        accessSet_3 = CreateSet('./data/test/test.json', '__test_question__')
        data = {
                "type": "__test_question__",
                "category": {
                    "Oil": {}
                    }
                }

        data_2 = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {
                            "next_id": 1,
                            "questions": []
                            }
                        }
                    }
                }
        data_3 = {
                "type": "__test_question__",
                "category": {
                    "Oil": {}
                    }
                }

        accessSet.data = data
        accessSet_2.data = data_2
        accessSet_3.data = data_3

        json_cmp = {
                "type": "__test_question__",
                "category": {}
                }


        accessSet.removeCategory("Oil")
        accessSet_2.removeCategory("Oil")
        self.assertEqual(accessSet.data, json_cmp)
        self.assertEqual(accessSet_2.data, json_cmp)

        self.assertRaises(KeyError, accessSet_3.removeCategory, "Well")

    def test_removeGroup(self):
        accessSet = CreateSet("./data/test/test.json", "__test_question__")

        data = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {"next_id":1, "questions": []}
                        }
                    }
                }

        accessSet.data = data

        json_cmp = {
                "type": "__test_question__",
                "category": {
                    "Oil": {}
                    }
                }

        accessSet.removeGroup("Header", "Oil")
        self.assertEqual(accessSet.data, json_cmp)

    def test_removeQuestion(self):
        accessSet = CreateSet("./data/test/test.json", "__test_question__")

        data = {
                "type": "__test_question__",
                "category": {
                    "Oil": {
                        "Header": {
                            "next_id":2, "questions": [{"q_id":1, "question":"Cover Page"}]
                        }
                    }
                }
                }

        accessSet.data = data
        json_cmp = {
            "type": "__test_question__",
            "category": {
                "Oil": {
                    "Header": {"next_id":2, "questions": []}
                    }
                }
            }

        accessSet.removeQuestion("Cover Page", "Header", "Oil")
        self.assertEqual(accessSet.data, json_cmp)







if __name__ == '__main__':
    unittest.main()
