import json
import logging

from modelData.Question import Question

translator_logger = logging.getLogger('manager_application.translator')

class JsonToObject():

    def __init__(self, file):
        self.file = file
        self.logger = logging.getLogger('manager_application.translator.JsonToObject')
        self.logger.info("creating an instance of translator")

    # Creates Question Objects from data in given file
    def convertAllObjects(self):
        self.logger.info("converting all prompts")
        quesObjects = []
        with open(self.file, "r") as read_file:
            data = json.load(read_file)
            categories = data["category"]
            for category in categories:
                groups = categories[category]
                for group in groups:
                    questions = groups[group]["questions"]
                    for q in questions:
                        q_id = q["q_id"]
                        question = q["question"]
                        quesObj = self.convToObject(category, group, question)
                        quesObjects.extend([quesObj])

        return quesObjects

    def convertCategoryObjects(self, cat):
        self.logger.info("category to add: " + str(cat))
        try:
            quesObjects = []
            with open(self.file, "r") as read_file:
                data = json.load(read_file)
                category = data["category"][cat]
                for group in category:
                    self.logger.info("group to add: " + str(group))
                    questions = category[group]["questions"]
                    for q in questions:
                        q_id = q["q_id"]
                        question = q["question"]
                        quesObj = self.convToObject(cat, group, question)
                        quesObjects.extend([quesObj])

            return quesObjects
        except KeyError as e:
            logger.error(e)

    # Convert a single entity into a Question object
    def convToObject(self,category, group, question):
        self.logger.info("converting specific question: " + str(question))
        ques = Question(category, group, question)

        return ques
