import unittest
import json

from questionTranslator import JsonToObject
from modelData.Question import Question

class TestQuestionCreator(unittest.TestCase):
    def setUp(self):
        self.translator = JsonToObject('./dataManipulators/data/test/test_sample.json')

    def test_createQuestionObject(self):
        quesObj = self.translator.convToObject('Oil', 'Header', 'Cover Page')
        testObj = Question('Oil', 'Header', 'Cover Page')

        self.assertEqual(type(quesObj), type(testObj))

        self.assertEqual(quesObj.getPrompt(), testObj.getPrompt())
        self.assertEqual(quesObj.getCategory(), testObj.getCategory())
        self.assertEqual(quesObj.getGroup(), testObj.getCategory())

    def test_convertAllObjects(self):
        testObj = Question('Oil', 'Header', 'Howdy')
        testObjs = [testObj]
        quesObjs = self.translator.convertAllObjects()

        test_len = len(testObjs)
        ques_len = len(quesObjs)

        self.assertEqual(ques_len, test_len)

        self.assertEqual(quesObjs[0].getPrompt(), testObjs[0].getPrompt())
        self.assertEqual(quesObjs[0].getCategory(), testObjs[0].getCategory())
        self.assertEqual(quesObjs[0].getGroup(), testObjs[0].getGroup())



if __name__ == '__main__':
    unittest.main()
