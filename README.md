
# Project Review 
Allows to review projects based on given prompts that are have a yes/no answer or N/A.

# Software Architecture

## Module Approach
The software architecture follows a module approach, inspired by Linux's device driver implementation. (OS concept)
Where the core of the software lies in the **General Manager**. Connecting all other modules via this **General Manager**, allowing to add or remove modules based on desired functionality.

## MVC Approach
To make it scalable and easy to maintain.

# Components

### General Manager - *pivot*
It acts as the pivot for the whole software architecture. Without it, all other modules would not be able to communicate with one another.

Duty: Pass data that is desirable for all other modules.

###### Modules Used:

## Data Manipulators - **data layer**
Holds all modules that manage the data that is to be utilized by the software.

### data
Stores data in JSON file format.

..* data.json -> Main data used
..* test -> test data used for all test purposes.

##### ManageSetQuestions - *module*
Acts as an interface between the **data** file and the **General manager**.
Giving access to the prompts.

Maintains a copy of the data from the file, known as **Hard Prompts**, in a *dictionary* format

###### Modules Used:
..* Create Set

##### Create Set - *module*
Has direct access to the data file.

Can add/remove prompts in the file

## Model Data - **model**
Representation of the data in Python object.

..* Prompt (Base class)
..* Question

## Modules - **functionality**
Stores all modules that add certain functionality to the software. 
All connect to the General Manager to receive the desired data.

#### NOTE: 
All modules must convert the data received by the General Manager into the format that is adequate for the module.
Making it eaiser to implement and remove modules without affecting the General Manager.

### Report - **module**
Creates a report based on the given data

### Questionnaire - **module**
Starts a questionnaire to review the given data. Allowing for interactivity.