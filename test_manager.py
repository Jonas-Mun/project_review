from ProjectManager import GeneralManager
from modelData.Question import Question

import unittest
import json

class TestManager(unittest.TestCase):


    def test_manager_construct_volatile_prompts(self):

        manager = GeneralManager('./dataManipulators/data/test/test_sample.json', '__test_question__')
        q = Question("Oil", "Header", "Howdy")
        q.updateStatus('y')
        print(q.getStatus())

        test_prompts = [[q]]

        manager.constructCategoryObjects("Oil")
        manager.promptObjects[0][0].updateStatus("y")
        manager.produce_summary()
        manager.debug_display_summary()

        question = manager.promptObjects[0][0]

        self.assertEqual(question.getCategory(), q.getCategory())
        self.assertEqual(question.getGroup(), q.getGroup())
        self.assertEqual(question.getPrompt(), q.getPrompt())

    def test_new_prompts(self):
        # Create new json sample
        json_tmp = {
                "type": "__test_questions__",
                "category": {
                    "Oil": {
                        "Header": {
                            "next_id": 1, "questions": []
                            }
                        }
                    }
                }
        with open("./dataManipulators/data/test/tmp_sample.json", "w+") as outfile:
            json.dump(json_tmp, outfile)

        manager = GeneralManager("./dataManipulators/data/test/tmp_sample.json", "__test__")
        manager.add_new_prompt("Oil", "Header", "Cover Page")
        manager.add_new_prompt("Oil", "Header", "Table of Contents")
        manager.question_set_manager.confirm_changes()

        json_desire = {
                "type": "__test_questions__",
                "category": {
                    "Oil": {
                        "Header": {
                            "next_id" : 3,
                            "questions": [
                                {"q_id": 1, "question": "Cover Page"},
                                {"q_id": 2, "question": "Table of Contents"}
                                ]
                            }
                        }
                    }
                }

        with open("./dataManipulators/data/test/tmp_sample.json") as json_file:
            json_edited = json.load(json_file)
            self.assertEqual(json_edited, json_desire)




if __name__ == '__main__':
    unittest.main()
