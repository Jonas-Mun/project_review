from modelData.Question import Question
from modules.Questionnaire.Questionnaire import Questionnaire

ques_one = Question("Oil", "Header", "Cover Page")
ques_two = Question("Oil", "Header", "Title")

ques_list = [ques_one, ques_two]

bigQue = Questionnaire(ques_list)
bigQue.start_questionnaire("gui")

